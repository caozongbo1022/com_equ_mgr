#description from IXIA engineer:
#you can't use multiple ixload instances in a single tcl
#if an ixia port is being used by ixLoad, it cannot be used by IxNetwork or vise verse
namespace eval IxLD {
    set _ERROR ERROR
    set _SUCCESS SUCCESS
    package require IxLoad

    proc connect { { host " " } } {
        global testController
        ::IxLoad connect $host
        set testController [::IxLoad new ixTestController -outputDir 1]
        puts $IxLD::_SUCCESS
    }

    #-- disconnect to lib
    proc disconnect {} {
        global testController
        $testController releaseConfigWaitFinish
        ::IxLoad disconnect
        puts $IxLD::_SUCCESS
    }

    #--
    # Load the Config file (rxf)
    #--
    # Parameters:
    #       rxfpath: the config file absolute path
    proc loadConfig { rxfpath } {
       global repository
       #we send the file from linux to windows, so should verify it on linux
       #if { ![ file exists $rxfpath ] } {
       #   error "config (rxf) file not found..."
       #   return $IxLD::_ERROR
       #}
       set repository [ ::IxLoad new ixRepository -name $rxfpath ]
       puts $IxLD::_SUCCESS
   }

   proc getActiveTest {} {
      global repository
      set activeTest [ $repository  cget -activeTest ]
      return [ $repository testList.getItem $activeTest ]
   }

   proc runConfig {} {
        global repository
        global testController
        global stats_info_list
        set stats_info_list {}

        #set chassisChain [ $repository cget -chassisChain ]
        #$chassisChain refresh
        set activeTest    [ getActiveTest ]

        $activeTest config \
            -enableNetworkDiagnostics                    false \
            -statsRequired                               true \
            -showNetworkDiagnosticsAfterRunStops         false \
            -showNetworkDiagnosticsFromApplyConfig       false \
            -enableForceOwnership                        true
        set activeTest [ getActiveTest ]
        $activeTest clearGridStats
        $testController run $activeTest
        puts $IxLD::_SUCCESS
   }

   proc sendFile { src dst } {
        ::IxLoad sendFileCopy $src $dst
        puts $IxLD::_SUCCESS
   }


   proc stopConfig {} {
        global testController
        global NS
        vwait ::ixTestControllerMonitor
        ${NS}::StopCollector

        $testController stopRun
        global stats_info_list
        #puts "statsinfo:$stats_info_list"
        puts $IxLD::_SUCCESS
   }

   proc collectStats { args } {
        global stats_info_list
        set stats_info_list [lindex [lindex $args 1] 3]
   }

   proc selectStats { statList interval } {
        global testController
        global SelectedStats
        global NS

        set SelectedStats $statList

        set activeTest [ getActiveTest ]
        set NS statCollectorUtils
        set test_server_handle [$testController getTestServerHandle]
        ${NS}::Initialize -testServerHandle $test_server_handle
        ${NS}::ClearStats
        $activeTest clearGridStats

        set count 0
        foreach statItem $SelectedStats {
           set caption         [format "Watch_Stat_%s" $count]
           set statSourceType  [lindex $statItem 0]
           set statName        [lindex $statItem 1]
           set aggregationType [lindex $statItem 2]
           if { [ catch {
              ${NS}::AddStat \
               -caption            $caption \
               -statSourceType     $statSourceType \
               -statName           $statName \
               -aggregationType    $aggregationType \
               -filterList {} \
              } err ] } {
               puts "Add stats $statSourceType $statName error:$err"
               return $IxLD::_ERROR
           }
           incr count
        }
        ${NS}::StartCollector -command IxLD::collectStats -interval $interval
        puts $IxLD::_SUCCESS
   }

   proc getInstantStats { { proItem "" } } {
        global stats_info_list
        global SelectedStats

        set statslist $stats_info_list
        set statslen [llength $statslist]
        if { $statslen == 0 } {
            puts "stats length is zero"
            return $IxLD::_ERROR
        }

        set count 0
        set index -1
        foreach statItem $SelectedStats {
            set statName        [lindex $statItem 1]

            if { [ string match "*$proItem*" $statName ] > 0 } {
                set index $count
                break
            }
            incr count
        }

        if { $index == "-1" } {
                    puts "item not found"
                    return $IxLD::_ERROR
        }

        if { $index >= $statslen } {
                puts "item index out of boundary,index :$index len:$statslen"
                return $IxLD::_ERROR
        }
        set statsIndex [ lindex $stats_info_list $index ]
        set statsVal [ lindex $statsIndex 1 ]
        puts "key:$proItem value:$statsVal"
        return "ret:$statsVal:"
   }
}
