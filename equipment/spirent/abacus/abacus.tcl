#!/opt/active_tcl/bin/tclsh
package provide CalixAbacusApi 1.00
set auto_path [linsert $auto_path 0 /opt/abacus/APIClasses /opt/abacus/cmdline /opt/abacus/inifile ]
if { [catch {
    package require tclreadyapi
    package require tdom
    #package require Itcl
} err ] } {
    puts $err
} else {
    puts "package Abacus API has been loaded!"
}

::aba::ApiApplication app
::aba::ApiSystemInformation sysinfo
::aba::ApiTest test
::aba::ApiResults results
::aba::ApiReports reports

namespace eval cabacus {
    set _ERROR ERROR
    set _SUCCESS SUCCESS
}

proc ::cabacus::connect_to_chassis { chassis_ip aba_automation_server } {

    set conn [app Enter "$aba_automation_server"]
    if { ![regexp {sock\d+} $conn]} {
        set aba_error [sysinfo GetLastError]
        puts $cabacus::_ERROR
        puts $aba_error
        app Exit
        return $aba_error
    }

    if { [sysinfo SetConnection "$chassis_ip" ""] != 1 } {
        set aba_error [sysinfo GetLastError]
        puts $cabacus::_ERROR
        puts $aba_error
        app Exit
        return $aba_error
    }
    after 5000
    puts $cabacus::_SUCCESS
}


proc ::cabacus::_check_abacus_status { } {

    puts "Check Abacus Status"
    set result [app IsAbacusReady]
    set isempty [llength $result]
    if { $isempty == 0} then {
        puts "Abacus is not ready [app GetLastError]"
        return 0
    } else {
        puts "Abacus is ready"
        return 1
    }
}

proc ::cabacus::load_env_file { envfilename } {
    puts "Start to load env file-->$envfilename.\n"
    variable loadenverror
    set loadenverror [app Load "$envfilename"]
    puts $loadenverror
    if { ![string equal -nocase $loadenverror "OK"] } {
        set loadenverror [sysinfo GetLastError]
        puts "Unable to load the env file!\nError Info: $loadenverror"
        app Exit
        return $loadenverror
    } else {
        puts "Load env file success."
        puts $cabacus::_SUCCESS
    }
}

proc ::cabacus::start_test { } {
    puts "Abacus Starting Test..."
    variable startresult
    set res [test Start]
    puts $res
    if { ![string equal -nocase $res "OK"] } {
        set startresult [test GetLastError]
        app Exit
        puts $startresult
        puts $cabacus::_ERROR
        return 0
    }
    after 5000
    set waiting 1
    set timeout 10
    while { $waiting } {
        set testXml [test GetStatus]
        if {[regexp {\<env\-status\>RUNNING\<\/env\-status\>} $testXml]} {
            break
        } else {
            if {$waiting < $timeout } {
                after 10000
                incr waiting
            } else {
                puts $cabacus::_ERROR
                return 0
            }
        }
    }
    puts $cabacus::_SUCCESS
    return 1
}

proc ::cabacus::stop_test { {timeout 30} } {
    puts "Abacus Stopping Test..."
    set waiting 1
    #set timeout 30
    variable stopresult
    while { $waiting } {
        set test_xml [test GetStatus]
        if {[regexp {\<env\-status\>DONE\<\/env\-status\>} $test_xml]} {
            break
        } else {
            if { $waiting == 1 } {
                puts "The test is fail to stop after the specific duration time."
                puts "Trying to stop the test immediately...\n"
                if { ![string equal -nocase [test StopNow] "OK"] } {
                    set stopresult [test GetLastError]
                    puts "Error Info: $stopresult"
                    puts "Unable to stop the test!"
                    puts $cabacus::_ERROR
                    return $stopresult
                }
                after 5000
                set waiting [ expr {$waiting + 5}]
            }
            if {$waiting < $timeout } {
                after 5000
                #incr waiting
                set waiting [ expr {$waiting + 5}]
            } else {
                puts "Fail to stop the test."
                puts $cabacus::_ERROR
                return fail
            }
        }
    }
    puts "Stop the test successfully!\n"
    puts $cabacus::_SUCCESS
    return pass
}

proc default_template {} {
    set resetResult [reports ResetRGE]
    if {$resetResult==0} {
        puts "can not reset RGE"
        app Exit
        return "resetRGE fail"
    }
    puts "reset RGE successfully!\n"
    set getResult [::aba::GetRGETemplate]
    if {$getResult==0} {
        puts "can not reset RGE"
        app Exit
        return "GetRGE fail"
    }
    puts "get RGE successfully!\n"
    set setResult [reports SetRGEConfig $getResult]
    if {$setResult==0} {
        puts "can not set RGE"
        app Exit
        return "setRGE fail"
    }
    puts $cabacus::_SUCCESS
    return 1
}

proc ::cabacus::generate_report { } {
    puts "Generating Report..."
    default_template
    variable generateerror
    set res [reports Generate]
    if { ![string equal $res "OK"] } {
        puts "Unable to generate the test report."
        set generate_error [sysinfo GetLastError]
        app Exit
        puts $generate_error
        puts $cabacus::_ERROR
        return 0
    }
    puts "Report is successfully done!"
    puts $cabacus::_SUCCESS
    return 1
}

proc ::cabacus::download_report { {exception 0}} {
    variable folderName
    set folderName [clock format [clock seconds] -format %Y%m%d%H%M]
    puts $folderName
    set downloadresult [reports Download "/tmp/$folderName"]
    ::aba::CheckIfFailed $downloadresult "OK" app
    puts "Report is successfully downloaded!"

    set repfile [file join "/tmp/" $folderName "MyReport.xml"]
    puts "report file: $repfile"
    set fh [open "$repfile" r]
    if {$fh==""} {
        puts "Fail to open this file."
        puts $cabacus::_ERROR
        return 0
    }
    puts "file: $repfile is opened successfully !"
    puts $cabacus::_SUCCESS
    return $repfile
}

proc ::cabacus::disconnect_chassis {} {
    variable releaseresult
    set releaseresult [app Exit]
    app Exit
    if {$releaseresult!=0} {
        puts $cabacus::_ERROR
        return 0
    }
    puts "release abacus successfully!\n"
    puts $cabacus::_SUCCESS
    return 1
}
