from cafe.core.logger import CLogger as Logger
from cafe.core.exceptions.tg.abacus import *
from lxml import etree
import re

_module_logger = Logger(__name__)
debug = _module_logger.debug

class AbacusDriver(object):

    SUCCESS = "SUCCESS"
    ERROR = "ERROR"

    def __init__(self, session=None, name=None, app=None):

        super(AbacusDriver, self).__init__()
        self.session = session
        self.logger = self.session.logger

        self.chassis_ip = None
        self.aba_automation_server_ip = None
        self.report_file = None

    def __del__(self):
        self.session.close()

    def open(self, chassis_ip, aba_auto_server_ip):
        self.chassis_ip = chassis_ip
        self.aba_automation_server_ip = aba_auto_server_ip
        self.logger.info('Abacus Chassis IP: %s' % self.chassis_ip)
        self.logger.info('Abacus Automation Server IP: %s' % \
                         self.aba_automation_server_ip)

    def verify(self, response):
        flag = AbacusDriver.SUCCESS
        if flag not in response or "<error>:" in response:
            flag = AbacusDriver.ERROR
            return flag
        return flag

    def connect_to_chassis(self):

        res = self.session.command('::cabacus::connect_to_chassis %s %s '% \
                             (self.chassis_ip, self.aba_automation_server_ip))[2]
        self.logger.debug(res)
        if self.verify(res) != AbacusDriver.SUCCESS:
            raise AbacusConnectChassisError

    def disconnect_chassis(self):
        res = self.session.command('::cabacus::disconnect_chassis')[2]
        self.logger.debug(res)
        if self.verify(res) != AbacusDriver.SUCCESS:
            raise AbacusDisconnectChassisError

    def load_env_file(self, name):

        res = self.session.command('::cabacus::load_env_file %s'% name)[2]
        self.logger.debug(res)
        if self.verify(res) != AbacusDriver.SUCCESS:
            raise AbacusLoadEnvFileError

    def start_test(self):

        self.report_file = None
        res = self.session.command('::cabacus::start_test')[2]
        self.logger.debug(res)
        if self.verify(res) != AbacusDriver.SUCCESS:
            raise AbacusStartTestError

    def stop_test(self, timeout=30):

        res = self.session.command('::cabacus::stop_test %s'%(timeout))[2]
        self.logger.debug(res)
        if self.verify(res) != AbacusDriver.SUCCESS:
            raise AbacusStopTestError

    def generate_report(self):

        res = self.session.command('::cabacus::generate_report')[2]
        self.logger.debug(res)
        if self.verify(res) != AbacusDriver.SUCCESS:
            raise AbacusGenerateReportError

    def download_report(self):

        res = self.session.command('::cabacus::download_report')[2]
        self.logger.debug(res)
        if self.verify(res) != AbacusDriver.SUCCESS:
            raise AbacusDonwloadReportError

        m = re.search(r"file:\s+(\S+.xml)", res)
        if m:
            self.report_file = m.groups()[0]
        else:
            self.logger.error('Invalid log file format')
            raise AbacusDonwloadReportError

    def get_stats(self):
        try:
            if not self.report_file:
                self.generate_report()
                self.download_report()
        except Exception as e:
            self.logger.error(e)
            raise AbacusGetStatsError

        self.logger.debug('report file is: %s' % self.report_file)
        tree = etree.parse(self.report_file)
        m = tree.xpath("//stat-totals/total")
        if m:
            d = {k: m[0].get(k) for k in m[0].keys()}
            return d
        else:
            raise AbacusInvalidReportFormat
