from .proto.base import DriverBase


class WebRelayDriver(DriverBase):
    def __init__(self, session=None, name=None, app=None):
        self._session = session
        self.name = name
        self.app = app

    def toggle_switch(self, state, port):
        """
        This method is the low level driver for controlling a webrelay switch that is used on the
        webrelay version of fiber cut box.

        To close the switch port (connect the fiber) the state is set to 0
        To open the switch port (cut the fiber) the state is set to 1

        :param port: The port number to be toggled
        :param state: The state teh port is to be toggled to.
        :return: The switch state as a True or False
        """
        return self._session.toggle_switch(state, port)