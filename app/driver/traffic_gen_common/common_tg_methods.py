import collections
import re

import cafe
from cafe.core.logger import CLogger as Logger
from cafe.core.utils import Param


class TgCommon(object):
    SUCCESS = "SUCCESS"
    ERROR = "ERROR"

    _module_logger = Logger(__name__)
    debug = _module_logger.debug

    default_prompt = collections.OrderedDict(
        {r"[^\r\n].+\#": None,
         r"[^\r\n].+\>": None,
         r"[^\r\n].+\$": None,
         r"[^\r\n].+\:\~\$": None,
         r"[^\r\n]+(\%)": None,
         r"\-\-More\-\-": " ",
         }
    )
    error_response = r"error"

    def __init__(self, session, name, default_timeout, crlf, app):
        super(TgCommon, self).__init__()

        self.tcl = self.session = session
        self.name = name
        self.default_timeout = default_timeout
        self.crlf = crlf
        self.app = app

        self.current_prompt = None
        self._set_prompt(self.default_prompt)
        self.buf = ""
        self.msg = ""
        self.stats = {}
        self.handles = []

        # create attributes which is compatible with base class
        self.logger = self.session.logger
        self.chassis_ip = None
        self.port_list = None

    def _update_app_result(self, stats):
        """
        update the app module result data structure

        Args:
            stats (dict) - stats result of traffic generator
        """

        if self.app:
            d = {}
            d["session"] = self.name
            d["prompt"] = None
            d["content"] = str(stats)
            d["stats"] = stats

            # import pprint; pprint.pprint(d)
            self.app.update_result(d)

    def _from_handle_to_ref(self, handle):
        """
        Translate handle value to ref value
        If handle is not found in self.handle return handle itself
        """
        for h in self.handles:
            if handle == h.handle:
                return h.ref
        return handle

    def _flatten_and_translate(self, d, parent_key='', sep='.'):
        """
        flatten nested dictionary and translate value to traffic specific key values

        """
        items = []
        for k, v in d.items():
            # k is traffic gen object handle
            _k = self._from_handle_to_ref(k)
            new_key = '%s%s%s' % (parent_key, sep, _k) if parent_key else _k
            if isinstance(v, collections.MutableMapping):
                items.extend(self._flatten_and_translate(v, new_key, sep=sep).items())
            else:
                items.append((new_key, v))
        return dict(items)

    def get_handles(self, htype):
        """
        return a list of handle of same handle type
        """
        ret = []

        for h in self.handles:
            if h.handle_type == htype.lower():
                ret.append(h)
        return ret

    def del_handles(self):
        """
        return a list of handle of same handle type
        """
        for h in self.handles:
            if hasattr(self, h.ref):
                delattr(self, h.ref)
        self.handles = []

    def _set_prompt(self, d):
        self.prompt = d.keys()
        self.action = d.values()

    def _check_port(self, port):
        ports = self.get_handles('port')
        self.p = []
        map(lambda x: self.p.append(x.value), [y for y in ports])
        if str(port) not in self.p:
            raise ValueError('port %s not found' % port)

    def _check_stream(self, stream):
        s = self.get_handles('stream')
        ss = []
        for i in s:
            ss.append(i.value)
        if stream not in ss:
            raise ValueError('Stream %s not found!' % stream)

    def _process_stats(self, s):
        """
        extract the tcl result into python dictionary
        """
        if not self.SUCCESS in s:
            return Param({"status": -2})

        # parse the ret and convert into dictionary
        m = re.search(r"\#\#\#([^\#]+)\#\#\#", s)
        if m:
            x = m.group(1)
            p = Param()
            try:
                p.load_yaml_string(x)
                return p
            except TypeError:
                raise TypeError('invalid yaml file!')

        else:
            return Param({"status": -1})

    @cafe.teststep("get traffic gen stats by key regex")
    def get_stats_by_key_regex(self, key):
        ret = {}
        for k, v in self.stats.items():
            m = re.search(key, k)
            if m:
                ret[k] = v
        if ret:
            return ret
        else:
            raise RuntimeError("key %s not found!" % key)
